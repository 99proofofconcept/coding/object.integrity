﻿using CoreFx.Assertions;
using DataTransferObjects.Network.Validations;

namespace DataTransferObjects.Network.Assertions
{
    public static class NetworkAddressAssertions
    {
        public static EntityAssertions<NetworkAddress> BuildIntegrity(
            this NetworkAddress source,
            INetworkValidation networkValidation,
            Warnings warnings = null)
        {
           
            EntityAssertions<NetworkAddress> predicates = default(EntityAssertions<NetworkAddress>);
            
            if (source != null && networkValidation != null)
            {
                predicates = new EntityAssertions<NetworkAddress>()
                {
                    addr => 
                    {
                        bool isValid = networkValidation.IsValidIpAddress(addr.Ip);
                        if (!isValid && warnings != null) warnings.Lines.Add(nameof(addr.Ip));
                        return isValid;
                    } ,
                    addr => 
                    {
                         bool isValid = networkValidation.IsValidMacAddress(addr.Mac);
                         if (!isValid && warnings != null) warnings.Lines.Add(nameof(addr.Mac));
                         return isValid;
                    },
                    addr => 
                    {
                         bool isValid = networkValidation.IsValidSerieAddress(addr.Serie);
                         if (!isValid && warnings != null) warnings.Lines.Add(nameof(addr.Serie));
                         return isValid;
                    }
                };

                predicates.Source = source;
            }

            return predicates;
        }  
    }

   
}
