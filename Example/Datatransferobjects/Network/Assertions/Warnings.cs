﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace DataTransferObjects.Network.Assertions
{
    public class Warnings
    {
        public StringCollection Lines { get; set; }

        public Warnings() { Lines = new StringCollection(); }
    }
}
