﻿using System.Runtime.Serialization;

namespace DataTransferObjects.Network
{
    [DataContract]
    public class NetworkAddress
    {
        [DataMember]
        public string Ip { get; set; }
        [DataMember]
        public string Mac { get; set; }
        [DataMember]
        public string Serie { get; set; }
    }
}
