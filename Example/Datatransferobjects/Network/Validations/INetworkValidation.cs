﻿namespace DataTransferObjects.Network.Validations
{
    public interface INetworkValidation
    {
        bool IsValidIpAddress(string ip);
        bool IsValidMacAddress(string mac);
        bool IsValidSerieAddress(string serie);
    }
}
