﻿using System.Net;
using System.Text.RegularExpressions;

namespace DataTransferObjects.Network.Validations
{
    public class NetworkBasicValidations: INetworkValidation
    {
        public bool IsValidIpAddress(string ip)
        {
            IPAddress address;
            return IPAddress.TryParse(ip, out address);
        }

        public bool IsValidMacAddress(string mac)
        {
            var pattern = "^[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}$";
            return new Regex(pattern).IsMatch(mac);
        }

        public bool IsValidSerieAddress(string serie) => !string.IsNullOrEmpty(serie);
       
    }
}
