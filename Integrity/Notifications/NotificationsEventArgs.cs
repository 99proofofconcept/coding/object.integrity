﻿using System;
using System.Collections.Generic;

namespace CoreFx.Notifications
{
    public class NotificationsEventArgs : EventArgs
    {
        public List<string> Messeges { get; private set; }

        public NotificationsEventArgs(List<string> message)
        {
            Messeges = message;
        }
    }
}
