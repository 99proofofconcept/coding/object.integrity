﻿namespace CoreFx.Validations
{
    public interface IInjectValidation<T>
    {
        T SourceValidation { get; set; }
    }
}
